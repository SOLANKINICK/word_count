#Question:

Given a sentence, "the quick brown fox is quick and brown"
Use nodejs and objects in nodejs to find the frequency of each word in the given sentence
In this sentence
"the" has 1 occurences
"quick" has 2 occurences
"brown" has 2 occurences
and so on
As always, use git for version control

#Requirements:

We are required to produce a solution for the question
