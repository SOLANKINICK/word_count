function objH(str) {
  var hash = {};
  var s = str.split(" ");
  for(let i = 0; i < s.length; i++) {
    if (hash.hasOwnProperty(s[i])) {
      hash[s[i]]++;
    } else {
      hash[s[i]]=1;
    }
  }
  console.log(hash);
}

var str = "the quick brown fox is quick and brown";
objH(str);

